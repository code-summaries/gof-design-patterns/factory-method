<div align="center">
  <h1>Factory Method</h1>
</div>

<div align="center">
  <img src="factory_method_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Factory Method is a creational pattern that allows subclasses to alter the type of objects that
they create.**

### Real-World Analogy

_A car manufacturing plant._

Cars (abstract product) are built at a manufacturing plant (Abstract Creator).
The customer (client) orders a sports car (concrete product) at the Ferrari manufacturing plant (Concrete Creator).

### Participants

- :bust_in_silhouette: **Interface**
    - Provides an interface to:
        - `method` object (aka `other_method`)
    - Optionally provides an interface to:
        - `method` object

- :man: **ConcreteObject**
    - ...

**Product (Document)**- defines the interface of objects the factory method creates.
**ConcreteProduct (MyDocument)-** implements the Product interface.
**Creator (Application)**- declares the factory method, which returns an object of type Product. Creator may also define
a default implementation of the factory method that returns a default ConcreteProduct
object.- may call the factory method to create a Product object.•
**ConcreteCreator (MyApplication)**- overrides the factory method to return an instance of a ConcreteProduct.

### Collaborations

...

Creator relies on its subclasses to define the factory method so that it returns an instance of the appropriate
ConcreteProduct

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When ... .**

• a class can't anticipate the class of objects it must create.
• a class wants its subclasses to specify the objects it creates.
• classes delegate responsibility to one of several helper subclasses, and you want to localize the knowledge
of which helper subclass is the delegate.

• You don’t know beforehand the exact types and dependencies of the objects your code should work with
• Provide users of your library or framework with a way to extend its internal components
• Save system resources by reusing existing objects instead of rebuilding them each time

### Motivation

- ...

Consider a framework for applications that can present multiple documents to the user. Two key abstractions in this
framework are the classes Application and Document.
the Application class can't predict the subclass of Document to instantiate—the Application class only knows when a new
document should be created, not what kind of Document to create.
This creates a dilemma: The framework must instantiate classes, but it only knows about abstract classes, which it
cannot instantiate.

---
Frameworks use abstract classes to define and maintain relationships between
objects. A framework is often responsible for creating these objects as well.

Consider a framework for applications that can present multiple documents to
the user. Two key abstractions in this framework are the classes Application and
Document. Both classes are abstract, and clients have to subclass them to realize
their application-specific implementations. To create a drawing application, for
example,we define the classes DrawingApplication and DrawingDocument.The
Application classis responsible for managing Documents and will create them as
required—when the user selects Open or New from a menu, for example.

Because the particular Document subclassto instantiate is application-specific,the
Application class can't predict the subclass of Document to instantiate—the Application class only knows when a new
document should be created, not what kind
of Document to create. This creates a dilemma: The framework must instantiate
classes, but it only knows about abstract classes, which it cannot instantiate.

The Factory Method pattern offers a solution. It encapsulates the knowledge
of which Document subclass to create and moves this knowledge out of the
framework.

application subclasses redefine an abstract CreateDocument operation on Application to return the appropriate Document
subclass. Once an Application subclass is instantiated, it can then instantiate application-specific Documents without
knowing their class. We call CreateDocument a factory method because it's
responsible for "manufacturing" an object.

---
Use the Factory Method when you don’t know beforehand the exact types and dependencies of the objects your code should
work with.

The Factory Method separates product construction code from the code that actually uses the product. Therefore it’s
easier to extend the product construction code independently from the rest of the code.

For example, to add a new product type to the app, you’ll only need to create a new creator subclass and override the
factory method in it.

Use the Factory Method when you want to provide users of your library or framework with a way to extend its internal
components.

Inheritance is probably the easiest way to extend the default behavior of a library or framework. But how would the
framework recognize that your subclass should be used instead of a standard component?

The solution is to reduce the code that constructs components across the framework into a single factory method and let
anyone override this method in addition to extending the component itself.

Let’s see how that would work. Imagine that you write an app using an open source UI framework. Your app should have
round buttons, but the framework only provides square ones. You extend the standard Button class with a glorious
RoundButton subclass. But now you need to tell the main UIFramework class to use the new button subclass instead of a
default one.

To achieve this, you create a subclass UIWithRoundButtons from a base framework class and override its createButton
method. While this method returns Button objects in the base class, you make your subclass return RoundButton objects.
Now use the UIWithRoundButtons class instead of UIFramework. And that’s about it!

Use the Factory Method when you want to save system resources by reusing existing objects instead of rebuilding them
each time.

You often experience this need when dealing with large, resource-intensive objects such as database connections, file
systems, and network resources.

Let’s think about what has to be done to reuse an existing object:

First, you need to create some storage to keep track of all of the created objects.
When someone requests an object, the program should look for a free object inside that pool.
… and then return it to the client code.
If there are no free objects, the program should create a new one (and add it to the pool).
That’s a lot of code! And it must all be put into a single place so that you don’t pollute the program with duplicate
code.

Probably the most obvious and convenient place where this code could be placed is the constructor of the class whose
objects we’re trying to reuse. However, a constructor must always return new objects by definition. It can’t return
existing instances.

Therefore, you need to have a regular method capable of creating new objects as well as reusing existing ones. That
sounds very much like a factory method.

---

by encapsulating the pizza creation in one class, we now have only one place to make modifications when the
implementation change

### Known Uses

- Plugin Systems and Extensions: Frameworks or systems that allow the addition of plugins or extensions use the Factory
  Method pattern to instantiate specific plugin instances based on configuration or user input.

Abstracting Object Creation: When creating instances of objects and the specific subclass to be instantiated needs to be
determined dynamically, Factory Methods help in encapsulating the creation logic.

Dependency Injection Containers: Dependency injection frameworks often employ Factory Methods to create instances of
classes that are registered in the container, allowing for flexibility in object creation.

Database Access and ORM: Object-Relational Mapping (ORM) frameworks utilize Factory Methods to create instances of
mapped objects or entities retrieved from a database.

Localization and Internationalization: Factory Methods assist in creating localized resources or objects specific to
different languages or regions at runtime based on user preferences or system settings.

File or Resource Loading: In scenarios where various file types or resources need to be loaded differently, Factory
Methods are used to create objects for handling the loading process based on file types.

Document and Report Generation: Generating different types of documents or reports dynamically based on templates or
configurations is facilitated by the Factory Method pattern.

Game Development: Games often use the Factory Method pattern to create different types of game objects (characters,
weapons, items) depending on game state or level requirements.

Caching Strategies: Implementing different caching strategies (like lazy loading, eager loading) using Factory Methods
to create appropriate caching instances based on the required strategy.

Logging Systems: Frameworks or libraries for logging can utilize Factory Methods to create logger instances tailored to
various logging requirements (file logging, console logging, database logging).

Creation of Threads or Tasks: In concurrent programming, Factory Methods can be used to create instances of threads or
tasks with specific configurations or behaviors.

Widget Creation in UI Libraries: Building user interface components where subclasses might create different types of UI
widgets (buttons, text fields) based on styles or themes uses the Factory Method pattern.

java.util.Calendar#getInstance()
java.util.ResourceBundle#getBundle()
java.text.NumberFormat#getInstance()
java.nio.charset.Charset#forName()
java.net.URLStreamHandlerFactory#createURLStreamHandler(String) (Returns singleton object per protocol)
java.util.EnumSet#of()
javax.xml.bind.JAXBContext#createMarshaller() and other similar methods

Factory methods pervade toolkits and frameworks. The preceding document ex-
ample is a typical use in MacApp and ET++ [WGM88]. The manipulator example
is from Unidraw.
Class View in the Smalltalk-80 Model/View/Controller framework has a method
defaultController that creates a controller, and this might appear to be a factory
method [Par90]. But subclasses of View specify the class of their default controller
by defining defaultControllerClass, which returns the class from which default-
Controller creates instances. So defaultControllerClass is the real factory method,
tKat is, the method that subclasses should override.
A more esoteric example in Smalltalk-80 is the factory method parserClass defined
by Behavior (a superclass of all objects representing classes). This enables a class
to use a customized parser for its source code. For example, a client can define
a class SQLParser to analyze the source code of a class with embedded SQL
statements. The Behavior class implements parserClass to return the standard
Smalltalk Parser class. A class that includes embedded SQL statements overrides
this method (as a class method) and returns the SQLParser class.
The Orbix ORB system from IONA Technologies [ION94] uses Factory Method to
generate an appropriate type of proxy (see Proxy (207)) when an object requests a
reference to a remote object. Factory Method makes it easy to replace the default
proxy with one that uses client-side caching, for example.

### Categorization

Purpose:  **Creational**  
Scope:    **Class**   
Mechanisms: **Inheritance**

Creational design patterns abstract the instantiation process.
They help make a system independent of how its objects are created, composed, and represented.
A class creational pattern uses inheritance to vary the class that's instantiated,
whereas an object creational pattern will delegate instantiation to another object.

There are two recurring themes in these patterns.
First, they all encapsulate knowledge about which concrete classes the system uses.
Second, they hide how instances of these classes are created and put together.
All the system at large knows about the objects is their interfaces as defined by abstract classes.
Consequently, the creational patterns give you a lot of flexibility in what gets created, who creates it, how it gets
created, and when.

### Aspects that can vary

- Subclass of object that is instantiated.

### Solution to causes of redesign

- Creating an object by specifying a class explicitly.
    - Instantiating concrete classes directly in client code makes it harder to reuse and change.

### Consequences

| Advantages                                         | Disadvantages                       |
|----------------------------------------------------|-------------------------------------|
| :heavy_check_mark: **Short description.** <br> ... | :x: **Short description.** <br> ... |

Factory methods eliminate the need to bind application-specific classes into your
code. The code only deals with the Product interface; therefore it can work with
any user-defined ConcreteProduct classes.
A potential disadvantage offactory methods is that clients might have to subclass
the Creator classjust to create a particular ConcreteProduct object. Subclassing is
fine when the client has to subclass the Creator class anyway, but otherwise the
client now must deal with another point of evolution.

1. Provides hooks for subclasses. Creating objects inside a class with a factory
   method is always more flexible than creating an object directly. Factory
   Method gives subclasses a hook for providing an extended version of an
   object.
   In the Document example, the Document class could define a factory method
   called CreateFileDialog that creates a default file dialog object for opening an
   existing document. A Document subclass can define an application-specific
   file dialog by overriding thisfactory method. In this case the factory method
   is not abstract but provides a reasonable default implementation.
2. Connects parallel class hierarchies. In the examples we've considered so far, the
   factory method is only called by Creators. But this doesn't have to be the
   case; clients can find factory methods useful, especially in the case of parallel
   class hierarchies.
   Parallel class hierarchies result when a class delegates some ofits responsibilities to a separate class. Consider
   graphical figures that can be manipulated
   interactively; that is, they can be stretched, moved, or rotated using the
   mouse. Implementing such interactions isn't always easy. It often requires
   storing and updating informationthat records the state of the manipulation
   at a given time. This state is needed only during manipulation; therefore
   it needn't be kept in the figure object. Moreover, different figures behave
   differently when the user manipulates them. For example, stretching a line
   figure might have the effect ofmoving an endpoint, whereas stretching a text
   figure may change its line spacing.
   With these constraints, it's better to use a separate Manipulator object that
   implements the interaction and keeps track of any manipulation-specific state
   that's needed. Different figures will use different Manipulator subclasses to
   handle particular interactions. The resulting Manipulator class hierarchy
   parallels (at least partially) the Figure class hierarchy:
   The Figure class provides a CreateManipulator factory method that lets
   clients create a Figure's corresponding Manipulator. Figure subclasses override thismethod to return an instance
   ofthe Manipulator subclass that'sright
   for them. Alternatively, the Figure class may implement CreateManipulator
   to return a default Manipulator instance, and Figure subclasses may simply
   inherit that default. The Figure classes that do so need no corresponding
   Manipulator subclass—hence the hierarchies are only partially parallel.
   Notice how the factory method defines the connection between the two class
   hierarchies. It localizes knowledge of which classes belong together

✅ Avoids coupling between concrete products and client code.
✅ Extract product creation code into one place (SRP).
✅ Add new products without breaking existing client code (OCP).
❌ Many new interfaces and classes are added with this pattern.

You avoid tight coupling between the creator and the concrete products.
Single Responsibility Principle. You can move the product creation code into one place in the program, making the code
easier to support.
Open/Closed Principle. You can introduce new types of products into the program without breaking existing client code.

The code may become more complicated since you need to introduce a lot of new subclasses to implement the pattern. The
best case scenario is when you’re introducing the pattern into an existing hierarchy of creator classes.

### Relations with Other Patterns

_Distinction from other patterns:_
_Combination with other patterns:_

- ...

**Abstract Factory (87)** is often implemented with factory methods. The Motivation example in the Abstract Factory
pattern illustrates Factory Method as well.
**Factory methods are usually called within Template Methods (325)**. In the document example above, NewDocument is a
template method.
**Prototypes (117)** don't require subclassing Creator. However, they often
require an Initialize operation on the Product class. Creator uses Initialize to initialize the object. Factory Method
doesn't require such an operation.

How do I make cli ents use the Proxy rather than the Real Subject?
One common technique is to provide a factory that instantiates and returns the subject.
we can then wrap the subject with a proxy before returning it. The client never knows or cares that it’s using a proxy
instead of the real thing

Wouldn’t it be easy for some client of a beverage to end up with a decorator that isn’t the outermost decorator?
decorators are typically created by using other patterns like Factory and Builder. Then the creation of the concrete
component with its decorator is “well encapsulated” and doesn’t lead to these kinds of problems.


<br>
<br>

## How do you apply it?

> :large_blue_diamond: **A creator class with an abstract method for creating products. And creator subclasses that
implement
that abstract create method.**

### Structure

```mermaid
classDiagram
    class Creator {
        <<abstract>>
        # FactoryMethod(): Product
        + AnOpperation()
    }

    class ConcreteCreator {
        + FactoryMethod(): Product
    }

    class Product {
        <<interface>>
    }

    class ConcreteProduct {
    }

    Creator <|-- ConcreteCreator: extends
    Product <|.. ConcreteProduct: implements
    ConcreteCreator --> ConcreteProduct: instantiates
```

### Variations

_Variation name:_

- **VariationA**: ...
    - :heavy_check_mark: ...
    - :x: ...
- **VariationB**: ...
    - :heavy_check_mark: ...
    - :x: ...

**Two major varieties**. The two main variations of the Factory Method pattern are (1) the case when the Creator class
is an abstract class and does not provide an implementation for the factory method it declares, and (2) the case when
the
Creator is a concrete class and provides a default implementation for the factory method.
The first case requires subclasses to define an implementation,because there's no reasonable default. It gets around the
dilemma of having to instantiate unforeseeable classes. In the second case, the concrete Creator uses the factory method
primarily for flexibility. It's following a rule that says, "Create objects in a separate operation so that subclasses
can override the way they're created." This rule ensures that designers of subclasses can change the class of objects
their parent class instantiates if necessary.

**Parameterized factory methods.** Another variation on the pattern lets the factory method create multiple kinds of
products. The factory method takes

### Implementation

In the example we apply the factory method pattern to a ... system.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Factory Method](https://refactoring.guru/design-patterns/factory-method)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [ZoranHorvat: Combine Design Patterns To Reveal Their Greatest Power](https://youtu.be/b1JyDzCe9n8?si=GAPwckCNdZzzo_Zo)
- [ZoranHorvat: Have you ever tried smart constructors?](https://youtu.be/mzq-XcTKxrg?si=eR-uNmVZN0ob8q9H)
- [Geekific: The Factory Method Pattern Explained and Implemented in Java](https://youtu.be/EdFq_JIThqM?si=oGkC2UWDuwOm1mDP)


<br>
<br>
